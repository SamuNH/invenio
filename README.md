
# Tech skill - INVENIO

Aplicación web en Laravel que poseé un conjunto reducido
de características propias de un Blog de Internet.

## Build with

* [Laravel 10](https://laravel.com/docs/10.x/installation#meet-laravel)
* [Tailwind CSS](https://tailwindcss.com/)
* [DaysiUI](https://daisyui.com/)

## Requirements

* [php 8.2](https://www.php.net/releases/8.2/en.php)
* [composer 2](https://getcomposer.org/download/)
* [npm latest-stable](https://nodejs.org/en/download/current)

* [enable pgsql](https://www.php.net/manual/en/pgsql.installation.php#:~:text=In%20order%20to%20enable%20PostgreSQL,ini%20or%20dl()%20function.)

## Installation

1.- Clone Invenio with **GIT** and moving inside

```bash
  git clone https://gitlab.com/SamuNH/invenio.git
  cd Invenio
```

2.- Checkout MASTER or DEVELOP branch

```bash
  git checkout (master or develop)
```

3.- Install COMPOSER packages

```bash
  composer install
```

4.- Install NPM packages

```bash
  npm install
```

5.- Check dotenv and put your own DB KEYS o leave current BD KEYS by naming .env.example to .env

6.- OPTIONAL: if changed your DB KEYS run

```bash
  php artisan migrate
```
    
## Deployment

To deploy this project run

```bash
  npm run dev
  php artisan serve
```

Access in http://127.0.0.1:8000

## API Reference

#### Get all posts

```http
  GET /api/posts
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `N/A`     | `N/A`    | `N/A`                      |

#### Get post

```http
  GET /api/posts/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `query: integer` | **Required**. Id of post to fetch |

#### Create post

```http
  POST /api/posts
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `title`   | `body: string, maxLength:30` | **Required**. Title of post   |
| `content` | `body: string, maxLength:250`| **Required**. Content of post |

#### Search post

```http
  GET /api/posts/${column}/${param}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `column`  | `query: string` | **Required**. Id of post to fetch |
| `param`   | `query: string` | **Required**. Id of post to fetch |
