<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostWebController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/register', [AuthController::class, 'store']);

Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'auth']);

Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

Route::get('/posts', [PostWebController::class, 'index'])->name('posts');
Route::post('/posts', [PostWebController::class, 'store'])->name('posts.create');
Route::get('/posts/{id}', [PostWebController::class, 'show'])->name('post');

Route::get('/create', [PostWebController::class, 'create'])->name('create');

Route::get('/search', [PostWebController::class, 'search'])->name('search');