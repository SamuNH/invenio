<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'title',
        'content',
        'author_id'
    ];

    protected $casts = [
        'created_at'  => 'date:Y/m/d'
    ];
    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];
}
