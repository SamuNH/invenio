<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::select('posts.id', 'posts.title', 'posts.content', 'posts.created_at AS creation', 'users.name AS author')->join('users', 'posts.author_id', '=', 'users.id')->get();
        return response()->json(['posts' => $posts], 200);
    }

    public function show($id)
    {
        $post = Post::select('posts.title', 'posts.content', 'posts.created_at AS creation', 'users.name AS author')->join('users', 'posts.author_id', '=', 'users.id')->where('posts.id', $id)->first();

        if (!$post) {
            return response()->json(['message' => 'Post not found'], 404);
        }

        return response()->json(['post' => $post], 200);
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'content' => 'required'
        ]);

        if(!$validated) return response()->json(['error' => $validated], 401);
        
        $user = Auth::user();

        $data = $request->all();
        $data['author_id'] = $user->id;

        $post = Post::create($data);

        return response()->json(['post' => $post], 201);
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if (!$post) {
            return response()->json(['message' => 'Post not found'], 404);
        }

        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'author_id' => 'required|exists:users,id',
        ]);

        $post->update($request->all());

        return response()->json(['post' => $post], 200);
    }

    public function destroy($id)
    {
        $post = Post::find($id);

        if (!$post) {
            return response()->json(['message' => 'Post not found'], 404);
        }

        $post->delete();

        return response()->json(['message' => 'Post deleted successfully'], 200);
    }

    public function search($column, $param)
    {
        if($column == "owner") {
            $column = 'author_id';
        }

        $param = User::select('id')->where("name", "ilike", "%$param%")->first();

        $post = Post::select('posts.id', 'posts.title', 'posts.content', 'posts.created_at AS creation', 'users.name AS author')
                ->join('users', 'posts.author_id', '=', 'users.id')
                ->where("posts.$column", 'ilike', "%$param%")
                ->get();

        return response()->json(['posts' => $post], 200);
    }

}