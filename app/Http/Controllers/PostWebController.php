<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class PostWebController extends Controller
{
    public function index()
    {
        /*$response = Http::get('http://your-api-url/posts');
        $posts = $response->json()['posts'];*/

        return view('pages.posts.index', compact('posts'));
    }

    public function show($id)
    {
        $request = Request::create("/api/posts/$id", 'GET');

        $response = Route::dispatch($request);

        $content = $response->getContent();

        $content = json_decode($content, true);

        $post = $content['post'];

        return view('pages.posts.show', compact('post'));
    }

    public function create()
    {
        return view('pages.posts.create');
    }

    public function store()
    {
        $validated = request()->validate(
            [
                'title' => 'required',
                'content' => 'required'
            ]);
        $data = [
            'title' => $validated['title'],
            'content' => $validated['content']
        ];
        $req = Request::create('api/posts', 'POST', $data);

        $response = Route::dispatch($req);
        $statusCode = $response->getStatusCode();


        if($statusCode == 201) return redirect()->route('dashboard')->with('success', 'Account created successfully!');

        return redirect()->route('create')->withErrors([
            'error' => 'Error: ' . $statusCode
        ]);
    }
    
    public function search()
    {
        $validated = request()->validate(
            [
                'column' => 'required',
                'param' => 'required'
            ]);

         $request = Request::create("/api/posts/" . $validated['column'] . "/" . $validated['param'], 'GET');
        $response = Route::dispatch($request);

        $content = $response->getContent();
        $content = json_decode($content, true);
        $posts = $content['posts'];

        return view('pages.admin.dashboard', compact('posts'));
    }


}