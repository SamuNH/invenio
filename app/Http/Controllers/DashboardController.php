<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


class DashboardController extends Controller
{
    public function index() {
        
        $request = Request::create('/api/posts', 'GET');

        $response = Route::dispatch($request);

        $content = $response->getContent();

        $content = json_decode($content, true);

        $posts = $content['posts'];

        return view('pages.admin.dashboard', compact('posts'));
    }
}
