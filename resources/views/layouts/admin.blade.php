<!doctype html>
<html>

<head>
    @include('includes.head')
</head>

<body>
    <div class="mx-auto">
        <header class="grid-rows-1">
            @include('includes.header')
        </header>
        <div id="main">
            <div class="grid grid-cols-4">
                @auth
                <div>
                    <ul class="menu bg-base-200 w-56 rounded-box">
                        <li>
                            <a href="{{ route('create') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="#ffffff" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round">
                                    <line x1="12" y1="5" x2="12" y2="19"></line>
                                    <line x1="5" y1="12" x2="19" y2="12"></line>
                                </svg>
                                ADD POST
                            </a>
                        </li>
                    </ul>
                </div>
                @endauth
                @guest
                <div class="bg-base-200">
                </div>
                @endguest
                <div class="col-span-3 bg-base-200 min-h-screen max-h-screen overflow-y-auto">
                    <div class="grid grid-cols-1 gap-4 justify-items-center mt-2">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="grid-rows-1">
        @include('includes.footer')
    </footer>
</body>

</html>
