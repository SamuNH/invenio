<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body>
        <div class="mx-auto">
            <header class="grid-rows-1">
            </header>
            <div id="main">                
                <div class="hero min-h-screen bg-base-200">
                    <div class="hero-content flex-col lg:flex-row-reverse">
                        <div class="text-center lg:text-left">
                            <h1 class="text-5xl font-bold">Join now and still posting!</h1>
                            <p class="py-6">Your community are excited and they are waiting for your newer postings.</p>
                        </div>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>