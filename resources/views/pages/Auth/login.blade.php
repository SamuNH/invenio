@extends('layouts.auth')
@section('content')
<div class="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
    <form class="card-body" action="{{ route('login') }}" method="post">
        @csrf
        <div class="tabs">
            <a class="tab tab-lg tab-lifted" href="{{ route('register') }}">Register</a>
            <a class="tab tab-lg tab-lifted tab-active">Login</a>
        </div>
        <div class="form-control">
            <input type="text" placeholder="Email" class="input input-bordered" name="email" id="email"
                required />
        </div>
        <div class="tooltip" data-tip="8 lenght">
            <div class="form-control">
                <input type="password" placeholder="Password" class="input input-bordered" name="password"
                    id="password" required />
            </div>
        </div>
        @error('auth')
        <div class="alert alert-error">
            <svg xmlns="http://www.w3.org/2000/svg" class="stroke-current shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>
            <span> {{ $message }} </span>
          </div>
        @enderror
        <div class="form-control mt-6">
            <button class="btn btn-primary">Login</button>
        </div>
    </form>
</div>
@endsection
