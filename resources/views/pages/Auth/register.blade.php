@extends('layouts.auth')
@section('content')
<div class="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
    <form class="card-body" action="{{ route('register') }}" method="post">
        @csrf
        <div class="tabs">
            <a class="tab tab-lg tab-lifted tab-active">Register</a>
            <a class="tab tab-lg tab-lifted" href="{{ route('login') }}">Login</a>
        </div>
        <div class="form-control">
            <input type="text" placeholder="Name" class="input input-bordered" name="name" id="name"
                required />
        </div>
        <div class="form-control">
            <input type="email" placeholder="Email" class="input input-bordered" name="email" id="email"
                required />
        </div>
        <div class="tooltip" data-tip="8 lenght">
            <div class="form-control">
                <input type="password" placeholder="Password" class="input input-bordered" name="password"
                    id="password" required />
            </div>
        </div>
        <div class="tooltip" data-tip="8 lenght">
            <div class="form-control">
                <input type="password" placeholder="Repeat password" class="input input-bordered"
                    name="password_confirmation" id="password_confirmation" required />
            </div>
        </div>
        <div class="form-control mt-6">
            <button class="btn btn-primary">Register</button>
        </div>
    </form>
</div>
@endsection
