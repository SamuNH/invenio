@extends('layouts.admin')
@section('content')
    @auth
        <div class="card card-side bg-base-100 shadow-xl mt-[8em]">
            <figure><img src="https://cdn1.byjus.com/wp-content/uploads/blog/2022/07/12104117/865357581_Mar22_STOCK-IMAGES-ADAPTS-For-BLOG-AAKASH-BROADCASTS-Set-2_Feature-Banner-1.jpg" alt="Movie"/></figure>
            <div class="card-body">
            <h2 class="card-title">
                {{ $post['title'] }}!
                <div class="badge badge-secondary"> {{ substr($post['creation'], 0, 10) }}</div>
            </h2>
            <p class="max-w-md break-words overflow-hidden"> {{ $post['content'] }} </p>
            <span class="text-[8pt]">Owner: </span><div class="badge badge-neutral badge-xs">{{ $post['author'] }}</div>
        </div>
      </div>
    @endauth
    @guest
        @include('includes.guest')
    @endguest
@endsection
