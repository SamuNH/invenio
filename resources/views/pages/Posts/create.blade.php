@extends('layouts.admin')
@section('content')
    @auth
        <div class="card w-full max-w-sm shadow-2xl bg-base-100 mt-[5em]">
            <form class="card-body" action="{{ route('posts.create') }}" method="post">
                @csrf
                <div class="form-control">
                    <label class="label">
                        <span class="label-text">Title</span>
                    </label>
                    <input type="text" placeholder="Your post title" class="input input-bordered" name="title" id="title"
                        required />
                </div>
                <div class="form-control">
                    <label class="label">
                        <span class="label-text">Content</span>
                    </label>
                    <textarea class="textarea textarea-primary" placeholder="Lorem impsun..." name="content" id="content" required></textarea>
                </div>
                <div class="form-control grid grid-cols-2">
                    <input type="text" class="input input-bordered input-xs w-full" disabled value="{{ auth()->user()->email }}"/>
                </div>
                <div class="form-control mt-6">
                    <button class="btn btn-primary">ADD</button>
                </div>
                @error('error')
                <div class="alert alert-error">
                    <svg xmlns="http://www.w3.org/2000/svg" class="stroke-current shrink-0 h-6 w-6" fill="none" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>
                    <span> {{ $message }} </span>
                  </div>
                @enderror
            </form>
        </div>
    @endauth
    @guest
        @include('includes.guest')
    @endguest
@endsection
