@extends('layouts.admin')
@section('content')
    @auth
        <div class="mx-auto mt-5">
            <form action="{{ route('search') }}" method="get">
                @csrf
                <div class="grid grid-cols-5">
                    <div class="col-span-3">
                        <input type="text" placeholder="Type here for search..." class="input input-bordered input-primary w-full input-xl" name="param" id="param"/>
                    </div>
                    <div class="col-span-1">
                        <select class="select select-primary w-full max-w-xs" name="column" id="column">
                            <option selected value="title">Title</option>
                            <option value="content">Content</option>
                            <option value="owner">Owner</option>
                        </select>
                    </div>
                    <div class="col-span-1">
                        <button class="btn btn-square btn-outline btn-primary">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#ffffff" stroke-width="1" stroke-linecap="round" stroke-linejoin="round"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        @forelse ($posts as $index => $post)
            <div class="card w-2/3 mt-[1em] mb-[1em] {{ $loop->index % 2 == 0 ? 'bg-primary text-primary-content' : 'bg-base-100 shadow-xl' }}">
                <div class="card-body">
                    <h2 class="card-title">
                        {{ $post['title'] }}
                        <div class="badge badge-secondary"> {{ substr($post['creation'], 0, 10) }}</div>
                    </h2>
                    <p>
                        {{ substr($post['content'], 0, 70) }}
                    </p>
                    <div class="card-actions justify-end">
                        <a href="{{ route('post', [ 'id' => $post['id'] ]) }}" class="btn {{ $loop->index % 2 == 0 ? '' : 'btn-primary' }}">Visit
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                stroke="#ffffff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                <path d="M5 12h13M12 5l7 7-7 7" />
                            </svg>
                        </a>
                    </div>
                    
            <span class="text-[8pt]">Owner: </span><div class="badge badge-neutral badge-xs">{{ $post['author'] }}</div>
                </div>
            </div>
        @empty
            <div class="hero bg-base-200 pt-[8em]">
                <div class="hero-content text-center">
                    <div class="max-w-md">
                        <h1 class="text-5xl font-bold">Ooops!</h1>
                        <p class="py-6">There is no post.</p>
                    </div>
                </div>
          </div>
        @endforelse
    @endauth
    @guest
        @include('includes.guest')
    @endguest
@endsection
