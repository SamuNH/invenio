<div class="col-span-4 hero min-h-screen bg-base-200">
    <div class="hero-content text-center">
        <div class="max-w-md">
            <h1 class="text-5xl font-bold">Hello there</h1>
            <p class="py-6">Sorry... but if you need to access this area, go to AUTHENTICATION SECTION and LOGIN.</p>
            <a class="btn btn-primary" href="{{ route('login') }}">Login</a>
        </div>
    </div>
</div>